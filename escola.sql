﻿CREATE TABLE Professor(
 id_professor SERIAL PRIMARY KEY,
 nome VARCHAR(20) NOT NULL,
 cpf CHAR(11) NOT NULL UNIQUE,
 matricula CHAR(24) NOT NULL UNIQUE);

 CREATE TABLE Aluno(
 id_aluno SERIAL PRIMARY KEY,
 cr DECIMAL(2,1) NOT NULL,
 nome VARCHAR(20) NOT NULL,
 cpf CHAR(11) NOT NULL UNIQUE,
 matricula CHAR(24) NOT NULL UNIQUE,
 endereco VARCHAR(150) NOT NULL
 fk_turma_id INTEGER NOT NULL,
 FOREIGN KEY (fk_turma_id) REFERENCES Turma(id_turma)
 );

 CREATE TABLE Turma(
 id_turma SERIAL PRIMARY KEY,
 serie VARCHAR(20) NOT NULL,
 qtd_alunos INTEGER NOT NULL,
 semestre INTEGER NOT NULL);

 CREATE TABLE Aula(
 id_aula SERIAL PRIMARY KEY,
 horario VARCHAR(15) NOT NULL);

 CREATE TABLE Disciplina(
 id_disciplina SERIAL PRIMARY KEY,
 nome VARCHAR(30) NOT NULL,
 ementa VARCHAR(300) NOT NULL);

 CREATE TABLE Leciona(
 id_leciona SERIAL PRIMARY KEY,
 fk_professor INTEGER NOT NULL,
 fk_disciplina INTEGER NOT NULL,
 fk_aula INTEGER NOT NULL,
 FOREIGN KEY (fk_professor) REFERENCES Professor(id_professor),
 FOREIGN KEY (fk_disciplina) REFERENCES Disciplina(id_disciplina),
 FOREIGN KEY (fk_aula) REFERENCES Aula(id_aula)
 );
